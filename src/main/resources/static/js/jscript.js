
// -------------------------------------------Web JS----------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
getLista()
var prefix = "myUl"
var pre = "myTa"
var idGlobal = 1




// Elimina tarea de la lista
function eliminarTarea(){
    var cierra = document.getElementsByClassName("cierra");
    for (var p = 0; p < cierra.length; p++) {
        cierra[p].onclick = function() {
            var div = this.parentElement;
            // console.log(div.id)
            div.style.display = "none";

            var idListTar = document.getElementsByClassName("visible")[0].id


            const dataObject = {
                idLista: parseFloat(idListTar.substr(4,2)),
                idTar: parseFloat(div.id.substr(4,2)),

            };
            // console.log(dataObject)
            deleteTarea(dataObject)
        }
    }
}


// Editar tarea de la lista
function editT(){
    var edit = document.getElementsByClassName("edit");
    for (var i = 0; i < edit.length; i++) {
        edit[i].onclick = function() {
            var div = this.parentElement;
            var editDIV = document.getElementById('editBoto')

            var tr = document.getElementsByClassName("editando")

            if (!editDIV.classList.contains('invisible')) {
                editDIV.classList.add('invisible')
            }
            else {
                if (tr == null){
                    editDIV.classList.remove("invisible")
                    div.classList.add('editando')
                }
                else {
                    for (let j = 0; j < tr.length; j++) {
                        tr[j].classList.remove('editando')
                    }
                    editDIV.classList.remove("invisible")
                    div.classList.add('editando')
                }
            }
        }
    }
}


function marcarTarea() {
    tareasArr = document.getElementsByClassName("draggable")
    console.log(tareasArr)
    for (let k = 0; k < tareasArr.length; k++) {
        tareasArr[k].onclick = function(li){
            const task = this.id;
            const idListTar = document.getElementsByClassName("visible")[0].id;
            
            console.log(this)

            if(this.classList.contains('checked')){
                const dataObject = {
                    idTar: parseFloat(task.substr(4,2)),
                    idLista: parseFloat(idListTar.substr(4,2)),
                    description: this.textContent,
                    done: false
                };
                console.log(dataObject)
                editTarea(dataObject)
                postTarea(dataObject)
            } else{
                const dataObject = {
                    idTar: parseFloat(task.substr(4,2)),
                    idLista: parseFloat(idListTar.substr(4,2)),
                    description: this.textContent,
                    done: true
                };
                console.log(dataObject)
                editTarea(dataObject)
                postTarea(dataObject)
            }
            li.target.classList.toggle('checked');
        }
    }
}



// Marcar la lista como visible
function agregarList() {
    var listas = document.getElementsByClassName("lista")
    var ulList = document.getElementsByClassName("dropzone")
    for (var i = 0; i < listas.length; i++) {
        listas[i].addEventListener("click", function () {
            var idList = ("myUl" + this.id)
            const el = document.getElementById(idList);
            const e = document.getElementById("myUl1");
            Sortable.create(el);
            Sortable.create(e);
            for (var i = 0; i < ulList.length; i++) {
                if (ulList[i].id === idList) {
                    ulList[i].classList.remove("invisible")
                    ulList[i].classList.add("visible")
                    listas[i].classList.add("activa")
                }
                else {
                    ulList[i].classList.remove("visible")
                    ulList[i].classList.add("invisible")
                    listas[i].classList.remove("activa")
                }
            }
        });
    }
}


// Crear una lista nueva
function  newList() {
    var dirList = document.createElement("div");


    var inputValue = document.getElementById("valueList").value;
    var t = document.createTextNode(inputValue);
    dirList.appendChild(t);
    if (inputValue === '') {
        alert("Inserta un texto para la lista.")
    } else {
        document.getElementById("listas").appendChild(dirList);
        document.getElementById("valueList").value = "";
        var ul = document.createElement("ul");


        const dataObject = {
            nombre: inputValue
        };

        postLista(dataObject)

        setTimeout(function(){
            getListID(inputValue)
            setTimeout(function (){
                console.log(idGlobal)

                if (idGlobal === 1){
                    dirList.className = "lista activa"
                    ul.className = "dropzone visible"
                }
                else{
                    dirList.className = "lista"
                    ul.className = "dropzone invisible"
                }

                dirList.setAttribute("id", idGlobal);

                ul.setAttribute("id", prefix + idGlobal)
                document.getElementById("task").appendChild(ul)
                setTimeout(function (){
                    agregarList()
                },100);
            }, 100);
        }, 100);
    }
}


// Crear una tarea nueva
function newTask() {
    try {
        var idListTar = document.getElementsByClassName("visible")[0].id
        var li = document.createElement("li");
        li.className = "draggable marcar"
        li.setAttribute("draggable", "true")
        var inputValue = document.getElementById("valueTask").value;
        var t = document.createTextNode(inputValue);
        li.appendChild(t);
        if (inputValue === '') {
            alert("Inserta un texto para la tarea.");
        } else {
            const dataObject = {
                idLista: parseFloat(idListTar.substr(4,2)),
                description: inputValue.toString(),
                done: false
            };
            postTarea(dataObject)

            setTimeout(function(){
                getTaskID(inputValue.toString())
                setTimeout(function (){
                    console.log(idGlobal)
                    li.setAttribute("id", pre + idGlobal)

                    document.getElementById(idListTar).appendChild(li);
                    document.getElementById("valueTask").value = "";
                    var span = document.createElement("SPAN");
                    var img = document.createElement('img');
                    img.src = "imatges/close_icon.jpg";
                    img.width = 20;
                    span.appendChild(img);
                    span.className = "cierra";
                    li.appendChild(span);
                    var span2 = document.createElement("SPAN");
                    var img2 = document.createElement('img');
                    img2.src = "imatges/edit_icon.jpg";
                    img2.width = 20;
                    span2.appendChild(img2);
                    span2.className = "edit";
                    li.appendChild(span2);

                    eliminarTarea()

                    editT()

                    marcarTarea()
                }, 100);
            }, 100);
        }
        // location.reload();
    } catch (error) {
        alert("Debes crear una lista primero i/o seleccionar una.")
    }
}

//Editar tarea
function editTareaBoton() {
    var task = document.getElementsByClassName("editando")[0].id
    var idListTar = document.getElementsByClassName("visible")[0].id
    var editT = document.getElementById("editText").value
    var boolStatus = document.getElementsByClassName("editando")[0].classList.contains('checked');

    // console.log(boolStatus)

    document.getElementById(task.toString()).innerHTML = editT.toString()

    var li = document.getElementById(task.toString());
    if (boolStatus){
        li.className = "draggable marcar checked"
    } else {
        li.className = "draggable marcar"
    }
    // li.setAttribute("id", pre + taskCount)
    li.setAttribute("draggable", "true")
    var inputValue = document.getElementById("valueTask").value;
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    var span = document.createElement("SPAN");
    var img = document.createElement('img');
    img.src = "imatges/close_icon.jpg";
    img.width = 20;
    span.appendChild(img);
    span.className = "cierra";
    li.appendChild(span);
    var span2 = document.createElement("SPAN");
    var img2 = document.createElement('img');
    img2.src = "imatges/edit_icon.jpg";
    img2.width = 20;
    span2.appendChild(img2);
    span2.className = "edit";
    li.appendChild(span2);

    const dataObject = {
        idTar: parseFloat(task.substr(4,2)),
        idLista: parseFloat(idListTar.substr(4,2)),
        description: editT.toString(),
        done: boolStatus.toString()
    };
    // console.log(dataObject)
    editTarea(dataObject)
}




// -------------------------------------------Heroku GET ITEMS----------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
class Lista{
    constructor(nombre) {
        this.id = id;
        this.nombre = nombre;
        this.tareaArrayList = tareaArrayList
    }
}

class Tarea {
    constructor(idTar, idLista, description, done) {
        this.idTar = idTar;
        this.idLista = idLista;
        this.description = description;
        this.done = done
    }
}
function getTaskID(nom) {
    fetch("listas")
        .then(resp => resp.json())
        .then(data => recorrerT(data, nom))
}

function recorrerT(list, nom){
    console.log(list)
    for (var i = 0; i < list.length; i++) {
        comparatorTareaName(list[i].tareaArrayList, nom)
    }
}

function comparatorTareaName(tasks, nom){
    console.log(tasks)
    for (var i = 0; i < tasks.length; i++) {
        if (tasks[i].description === nom){
            idGlobal = tasks[i].idTar
            console.log(idGlobal)
        }
    }
}

function getListID(nom) {
    fetch("listas")
        .then(resp => resp.json())
        .then(data => recorrerL(data, nom))
}

function recorrerL(list, nom){
    console.log(list)
    for (var i = 0; i < list.length; i++) {
        if (list[i].nombre === nom) {
            idGlobal = list[i].id
            console.log(idGlobal)
        }
    }
}






function getLista() {
    fetch("listas")
        .then(resp => resp.json())
        .then(data => renderListas(data))
}

function renderListas(list) {
    for (var i = 0; i < list.length; i++) {
        renderLista(list[i], i)
    }
}

function renderLista(item, i) {
    // console.log(JSON.stringify(item))
    var idL = item.id;
    var nombreL= item.nombre
    var taskList = item.tareaArrayList

    var dirList = document.createElement("div");
    if (i === 0){
        dirList.className = "lista activa"
    }
    else{
        dirList.className = "lista"
    }
    dirList.setAttribute("id", idL);
    var t = document.createTextNode(nombreL);
    dirList.appendChild(t);
    document.getElementById("listas").appendChild(dirList);
    var ul = document.createElement("ul");
    if (i === 0){
        ul.className = "dropzone visible"
    }
    else{
        ul.className = "dropzone invisible"
    }
    ul.setAttribute("id", prefix + idL)
    document.getElementById("task").appendChild(ul)
    list = document.querySelectorAll("li");
    for (var a = 0; a < list.length; a++){
        list[a].onclick = function(li){
            var task = this.id

            var idListTar = document.getElementsByClassName("visible")[0].id

            // console.log(this)
            var editT = this.textContent
            if(this.classList.contains('checked')){
                const dataObject = {
                    idTar: parseFloat(task.substr(4,2)),
                    idLista: parseFloat(idListTar.substr(4,2)),
                    description: editT.toString(),
                    done: false
                };
                // console.log(dataObject)
                editTarea(dataObject)
            } else{
                const dataObject = {
                    idTar: parseFloat(task.substr(4,2)),
                    idLista: parseFloat(idListTar.substr(4,2)),
                    description: editT.toString(),
                    done: true
                };
                // console.log(dataObject)
                editTarea(dataObject)
            }
            li.target.classList.toggle('checked');
        }
    }
    agregarList()
    // console.log(taskList.length)
    if (taskList !== null) {
        for (var x = 0; x < taskList.length; x++) {
            // console.log(taskList[x])
            renderTarea(taskList[x])
        }
    }
}

function renderTarea(element){
    var idT = element.idTar
    var idListaT = element.idLista
    var descT = element.description
    var isDone = element.done

    // taskCount++
    var li = document.createElement("li");
    if (isDone){
        li.className = "draggable marcar checked"
    } else {
        li.className = "draggable marcar"
    }
    li.setAttribute("id", pre + idT)
    li.setAttribute("draggable", "true")
    var t = document.createTextNode(descT);
    li.appendChild(t);
    document.getElementById(prefix + idListaT).appendChild(li);
    var span = document.createElement("SPAN");
    var img = document.createElement('img');
    img.src = "imatges/close_icon.jpg";
    img.width = 20;
    span.appendChild(img);
    span.className = "cierra";
    li.appendChild(span);
    var span2 = document.createElement("SPAN");
    var img2 = document.createElement('img');
    img2.src = "imatges/edit_icon.jpg";
    img2.width = 20;
    span2.appendChild(img2);
    span2.className = "edit";
    li.appendChild(span2);

    eliminarTarea()

    editT()

    marcarTarea()
}

// -------------------------------------------Heroku POST ITEMS----------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
function postLista(dataObject) {
    fetch("listas",{
        method:'POST',
        headers:{
            'Content-Type':'application/json'
        },
        body:JSON.stringify(dataObject)
    }).then(response=>{
        return response.json()
    })
}

function postTarea(dataObject) {
    var url = "listas/";
    var idURL = dataObject.idLista
    var tarea_url = "/tareas"

    var url_comp = url + idURL + tarea_url

    fetch(url_comp,{
        method:'POST',
        headers:{
            'Content-Type':'application/json'
        },
        body:JSON.stringify(dataObject)
    }).then(response=>{
        return response.json()
    })
}

// -------------------------------------------Heroku DELETE ITEMS----------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------
function deleteTarea(dataObject) {
    var url = "listas/";
    var idURL = dataObject.idList
    var idTAR = dataObject.idTar

    // console.log(idURL)
    // console.log(idTAR)
    var tarea_url = "/tareas/"

    var url_comp = url + idURL + tarea_url + idTAR

    fetch(url_comp, {
        method:'DELETE',
        headers:{
            'Content-Type':'application/json'
        },
        body:JSON.stringify(dataObject)
    }).then(response=>{
        return response.json()
    })
}

// -------------------------------------------Heroku EDIT ITEMS----------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------
function editTarea(dataObject) {
    var url = "listas/";
    var idURL = dataObject.idLista
    var tarea_url = "/tareas/"
    var idTAR = dataObject.idTar

    console.log(dataObject)

    var url_comp = url + idURL + tarea_url + idTAR

    fetch(url_comp,{
        method:'PUT',
        headers:{
            'Content-Type':'application/json'
        },
        body:JSON.stringify(dataObject)
    }).then(response=>{
        return response.json()
    })
}

