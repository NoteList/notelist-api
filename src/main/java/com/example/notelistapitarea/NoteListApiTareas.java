package com.example.notelistapitarea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteListApiTareas {

    public static void main(String[] args) {
        SpringApplication.run(NoteListApiTareas.class, args);
    }

}
