package com.example.notelistapitarea.controladores;

import com.example.notelistapitarea.modelo.entidades.Tarea;
import com.example.notelistapitarea.modelo.servicios.ServicioTareas;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ControladorTareas {
    private final ServicioTareas servicioTareas;

    @GetMapping("/listas/{idL}/tareas")
    public List<Tarea> listarTareas(){
        return servicioTareas.listarTareas();
    }

    @GetMapping("/listas/{idL}/tareas/{idTar}")
    public Tarea consultarTarea(@PathVariable long idTar){
        return servicioTareas.consultarTarea(idTar);
    }

    @PostMapping("/listas/{idL}/tareas")
    public Tarea crearTarea(@RequestBody Tarea newTarea) {
        return servicioTareas.crearTarea(newTarea);
    }

    @PutMapping("/listas/{idL}/tareas/{idTar}")
    public Tarea modificarTarea(@RequestBody Tarea mod){ return servicioTareas.modificarTarea(mod); }

    @DeleteMapping("/listas/{idL}/tareas/{idTar}")
    public Tarea eliminarTarea(@PathVariable long idTar) {
        return servicioTareas.eliminarTarea(idTar);
    }

}
