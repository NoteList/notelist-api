package com.example.notelistapitarea.controladores;

import com.example.notelistapitarea.modelo.entidades.Lista;
import com.example.notelistapitarea.modelo.servicios.ServicioListas;
import com.example.notelistapitarea.modelo.servicios.ServicioTareas;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ControladorListas {
    private final ServicioListas servicioListas;

    @GetMapping("/listas")
    public List<Lista> listarListas(){
        return servicioListas.listarLista();
    }

    @GetMapping("/listas/{id}")
    public Lista consultarTarea(@PathVariable Long id){
        return servicioListas.consultarLista(id);
    }

    @PostMapping("/listas")
    public Lista crearLista(@RequestBody Lista newLista) {
        return servicioListas.crearLista(newLista);
    }

    @PutMapping("/listas/{id}")
    public Lista modificarLista(@RequestBody Lista mod){
        return servicioListas.modificarLista(mod);
    }

    @DeleteMapping("/listas/{id}")
    public Lista eliminarLista(@PathVariable Long id) {
        return servicioListas.eliminarLista(id);
    }

}
