package com.example.notelistapitarea.modelo.servicios;

import com.example.notelistapitarea.modelo.entidades.Tarea;
import com.example.notelistapitarea.modelo.repositorios.RepositorioTareas;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ServicioTareas {
    private final RepositorioTareas repositorioTareas;

    //Listar las tareas
    public List<Tarea> listarTareas(){
        return repositorioTareas.findAll();
    }
    //Consulatar las tareas
    public Tarea consultarTarea(long idTar){
        return repositorioTareas.findById(idTar).orElse(null);
    }

    //Añadir tarea
    public Tarea crearTarea(Tarea it){
        return repositorioTareas.save(it);
    }

    //Modificar tarea, si existe el canvia, sino retorna null
    public Tarea modificarTarea(Tarea it){
        Tarea aux=null;
        if(repositorioTareas.existsById(it.getIdTar())) aux=repositorioTareas.save(it);
        return aux;
    }

    //Eliminar Tarea
    public Tarea eliminarTarea(long idTar){
        Tarea res=repositorioTareas.findById(idTar).orElse(null);
        if(res!=null) repositorioTareas.deleteById(idTar);
        return res;
    }
}
