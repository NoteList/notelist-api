package com.example.notelistapitarea.modelo.servicios;

import com.example.notelistapitarea.modelo.entidades.Lista;
import com.example.notelistapitarea.modelo.entidades.Tarea;
import com.example.notelistapitarea.modelo.repositorios.RepositorioListas;
import com.example.notelistapitarea.modelo.repositorios.RepositorioTareas;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ServicioListas {
    private final RepositorioListas repositorioListas;

    //Listar las Listas
    public List<Lista> listarLista(){
        return repositorioListas.findAll();
    }
    //Consulatar las listas
    public Lista consultarLista(Long id){
        return repositorioListas.findById(id).orElse(null);
    }

    //Añadir Lista
    public Lista crearLista(Lista it){
        return repositorioListas.save(it);
    }

    //Modificar lista, si existe el canvia, sino retorna null
    public Lista modificarLista(Lista it){
        Lista aux=null;
        if(repositorioListas.existsById(it.getId())) aux=repositorioListas.save(it);
        return aux;
    }

    //Eliminar Lista
    public Lista eliminarLista(Long id){
        Lista res=repositorioListas.findById(id).orElse(null);
        if(res!=null) repositorioListas.deleteById(id);
        return res;
    }
}
