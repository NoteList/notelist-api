package com.example.notelistapitarea.modelo.entidades;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity

public class Lista {
    @Id
    @GeneratedValue( strategy =GenerationType.AUTO )
    private long id;
    private String nombre;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "idLista")
    private List<Tarea> tareaArrayList;
}