package com.example.notelistapitarea.modelo.entidades;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Tarea {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private long idTar;
    private long idLista;
    private String description;
    private boolean done;
}