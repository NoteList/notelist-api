package com.example.notelistapitarea.modelo.repositorios;

import com.example.notelistapitarea.modelo.entidades.Tarea;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorioTareas extends JpaRepository<Tarea, Long> {
}

