package com.example.notelistapitarea.modelo.repositorios;

import com.example.notelistapitarea.modelo.entidades.Lista;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorioListas extends JpaRepository<Lista, Long> {
}

