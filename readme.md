# NOTE LIST
## Integrantes del equipo:
Joan Brun, Marc Botana y Oriol Sanchez
## Descripcion de la página web:
<p>La página web consta de un muro donde se suben todas las tareas, en ese muro
puedes marcar y desmarcal las tareas hechas, tambien tiene una función de ordenar dichas tareas 
mediante drag and drop y por ultimo una funcion que permite eliminar las tareas definitivamente.</p>

## Descripcion de la API:
<p>Desde Postman podemos probar las diferentes funcionalidades que podremos 
encontrar en la web, cuando estas estén implementadas.</p>

## Atributos del element item:
<p>
-String id<br>
-String description<br>
-Boolean done
</p>


### Las funcionalidades de la API:

<ul>
<li>Crear una nueva tarea: con la ruta /tareas, poniendo como metodo de envio POST</li>
<li>Listar items: con la ruta /tareas, poniendo como metodo de envio GET</li>
<li>Consultar item determinado: con la ruta /tareas/{id}, poniendo como metodo de envio GET</li>
<li>Modificar item: con la ruta /tareas/{id}, poniendo como metodo de envio PUT</li>
<li>Eliminar item: con la ruta /tareas/{id}, poniendo como metodo de envio DELETE</li>
</ul>
